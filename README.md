# Travel Agency
Integration of mockup during HTML/CSS integration work. Personnal challenge, use Vue.js framework.

## ✅  Launch project
```
./start.sh
```
Check if you are correct rights on start.sh file.
This file use docker for create a container, [please install it](https://www.docker.com/get-started).

### See container logs
```
docker logs -f Front
```

## 🔧 Functionalities
- Destinations
- Deals
- Cards
- Contact
- Router
- SASS support

## 🚨  Dependencies
- [sass-loader](https://www.npmjs.com/package/sass-loader)
- [vue-router](https://www.npmjs.com/package/vue-router)
- [vuex](https://www.npmjs.com/package/vuex)

## 👤 Creator
[Rémi RUBIS](https://gitlab.com/remirubis)