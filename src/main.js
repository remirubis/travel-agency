import { createApp } from "vue";
import App from "./App.vue";
import { createStore } from 'vuex';
import router from './router';

const store = createStore({
	state: {
    destinations: [
      {
        id: 0,
        dealOfTheDay: true,
        deal: true,
        pictures: ["973.webp", "924.webp", "841.webp", "408.webp", "1042.webp", "220.webp", "984.webp", "1036.webp", "974.webp"],
        title: "Amsterdam",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 65
      }, {
        id: 1,
        deal: false,
        pictures: ["106.webp", "924.webp", "841.webp", "408.webp", "1050.webp", "939.webp", "984.webp", "1036.webp", "974.webp", "76.webp"],
        title: "Sydney",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 230
      }, {
        id: 2,
        deal: false,
        pictures: ["202.webp", "924.webp", "841.webp", "408.webp", "826.webp", "1050.webp", "1042.webp", "220.webp", "984.webp", "1036.webp", "974.webp"],
        title: "Rio",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 200
      }, {
        id: 3,
        deal: false,
        pictures: ["375.webp", "924.webp", "841.webp", "408.webp", "826.webp", "1050.webp", "939.webp", "1042.webp", "220.webp", "984.webp", "1036.webp", "974.webp", "76.webp"],
        title: "Cusco",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 145
      }, {
        id: 4,
        deal: false,
        pictures: ["563.webp", "924.webp", "841.webp", "408.webp", "826.webp", "1050.webp", "939.webp", "1042.webp", "220.webp", "984.webp", "1036.webp", "974.webp", "76.webp"],
        title: "New Delhi",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 160
      }, {
        id: 5,
        deal: true,
        pictures: ["689.webp", "924.webp", "841.webp", "408.webp", "826.webp", "1050.webp", "974.webp", "76.webp"],
        title: "Hong Kong",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 122
      }, {
        id: 6,
        deal: true,
        pictures: ["851.webp", "924.webp", "841.webp", "408.webp", "826.webp", "1050.webp", "939.webp", "1042.webp", "220.webp", "984.webp", "1036.webp", "974.webp", "76.webp"],
        title: "Katmandou",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 98
      }, {
        id: 7,
        deal: false,
        pictures: ["1038.webp", "924.webp", "408.webp", "826.webp", "939.webp", "1042.webp", "220.webp", ],
        title: "Tokyo",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 180
      }, {
        id: 8,
        deal: true,
        pictures: ["1057.webp", "841.webp", "408.webp", "826.webp", "1050.webp", "939.webp", "1042.webp", "1036.webp", "974.webp", "76.webp"],
        title: "Lisbonne",
        subtitle: "A beautiful city for you and your family.",
        hotel: "Hotel Calypso",
        duration: "1 week",
        desc: "Donec vel suscipit dolor, id efficitur ante. Sed et lectus urna. Vivamus tempus posuere tristique. Nunc pulvinar ligula sit amet pretium vehicula. Aliquam at tristique turpis. Phasellus semper placerat mi lobortis ultrices. Aliquam id commodo nulla. Nunc ut consectetur purus. Curabitur vulputate condimentum cursus.",
        content: "<h3>Description</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis imperdiet tortor finibus iaculis. Nunc varius tortor in auctor maximus. Suspendisse suscipit sem sit amet urna rutrum efficitur. Donec sem lacus, egestas id dolor id, blandit posuere diam. Curabitur in leo tempus, interdum lectus in, consectetur lacus. Etiam tincidunt nulla id imperdiet maximus. Cras elementum vitae ante non cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel massa gravida, cursus massa vitae, fermentum purus. Proin tincidunt ornare tincidunt. Sed vitae est id ante commodo malesuada. Aliquam erat volutpat. Cras porta felis massa, nec egestas dui blandit sit amet. Fusce nec dolor blandit, porttitor leo et, tempor turpis. Donec et sapien justo. Aenean a elit finibus, elementum eros ac, auctor libero.</p><h3>Details</h3><p>Vivamus a efficitur augue, a suscipit est. Vestibulum facilisis fringilla ultricies. Curabitur quis purus risus. Morbi urna lacus, euismod sit amet volutpat eget, efficitur ut enim. Pellentesque quis dictum quam. Nunc est ligula, ornare sed nisi non, sollicitudin efficitur lorem. Praesent rutrum, mi vitae tempus tempor, justo metus porta urna, vel bibendum magna odio non sem. Sed non massa at tellus aliquam faucibus.</p><h3>Other</h3><p>Nullam rutrum diam ut neque bibendum tincidunt. In eget dui ullamcorper tellus ultricies tempor sed eget leo. Suspendisse ut arcu et urna vehicula venenatis vel volutpat sapien. In eget eros justo. Donec sit amet lectus quam. Etiam vehicula dignissim varius. Integer consectetur pretium nulla, quis maximus tellus aliquam ut. Mauris rhoncus, nisi sed posuere dapibus, nulla magna auctor odio, vitae efficitur turpis enim ut nibh. Etiam bibendum dapibus enim in posuere. Donec eget nisl ut libero eleifend sollicitudin. Nulla imperdiet consectetur augue. Suspendisse metus leo, consectetur et cursus non, auctor at dui. Nulla id erat massa. Nullam porta turpis eu dignissim vehicula. Maecenas iaculis ex dui, eu rhoncus mauris aliquet pulvinar. Cras quis venenatis diam.</p>",
        price: 133
      }
    ],
    agency: [
      {
        value: 'paris',
        name: 'Paris',
        address: '12 rue Dupont, 75020 Paris'
      },
      {
        value: 'lyon',
        name: 'Lyon',
        address: '12 rue Dupont, 69001 Lyon'
      },
      {
        value: 'marseille',
        name: 'Marseille',
        address: '12 rue Dupont, 13001 Marseille'
      },
      {
        value: 'lille',
        name: 'Lille',
        address: '12 rue Dupont, 59000 Paris'
      },{
        value: 'la-rochelle',
        name: 'La Rochelle',
        address: '12 rue Dupont, 17000 La Rochelle'
      }

    ]
  },
});

const app = createApp(App);
app.use(router);
app.use(store);
app.mount("#app");
