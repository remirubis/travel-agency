import { createRouter, createWebHistory } from 'vue-router'
import Homepage from '../views/Homepage';
import Deals from '../views/Deals';
import Destinations from '../views/Destinations';
import SingleDestination from '../views/SingleDestination';
import Contact from '../views/Contact';
import Terms from '../views/Terms';
import NotFound from '../views/NotFound';

const routes = [
  {
    path: '',
    alias: '/home',
    name: 'home',
    component: Homepage,
    meta: {
      title: 'Home Page'
    }
  },
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/destinations',
    name:'destinations',
    component: Destinations,
    meta: {
      title: 'Our destinations'
    }
  },
  {
    path: '/destination',
    redirect: '/destinations'
  },
  {
    path: '/destination/:id',
    name: 'single-destination',
    component: SingleDestination
  },
  {
    path: '/deals',
    name:'deals',
    component: Deals,
    meta: {
      title: 'Our deals'
    }
  },
  {
    path: '/contact',
    name:'contact',
    component: Contact,
    meta: {
      title: 'Contact us'
    }
  },
  {
    path: '/terms',
    name: 'terms',
    component: Terms,
    meta: {
      title: 'Terms & Conditions'
    }
  },
  {
    path: "/:catchAll(.*)",
    redirect: "/404"
  },
  {
    path: '/404',
    name: '404',
    component: NotFound,
    meta: {
      title: 'Hmm page not found'
    }
}
]

const router = createRouter({
  history: createWebHistory(process.enBASE_URL),
  routes
})

export default router
