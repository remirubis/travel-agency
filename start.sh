echo "Running Travel-Agency";
docker run --name "Front" -it -d -v ${PWD}:/app -v /app/node_modules -p 8081:8080 -e CHOKIDAR_USEPOLLING=true --rm remirubis/travel-agency
echo "Done."